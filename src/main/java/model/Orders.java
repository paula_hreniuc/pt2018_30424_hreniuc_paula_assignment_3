package model;

/**
 * This class contains variables that correspond to the fields of the orders table from the database (have the same name), 2 constructors and setter and getters for each field
 * @author Paula Hreniuc
 *
 */
public class Orders {
	private int id_order;
	private int client_id;
	private int product_id;
	private int quantity;
	
	/**
	 * Constructor 
	 * @param o_id
	 * @param c_id
	 * @param p_id
	 * @param quantity
	 */
	public Orders(int o_id, int c_id, int p_id, int quantity){
		super();
		this.id_order = o_id;
		this.client_id = c_id;
		this.product_id = p_id;	
		this.quantity = quantity;
	}
	
	/**
	 * Constructor
	 * @param c_id
	 * @param p_id
	 * @param quantity
	 */
	public Orders(int c_id, int p_id, int quantity){
		super();
		this.client_id = c_id;
		this.product_id = p_id;	
		this.quantity = quantity;
	}
	
	/**
	 * setIDOrder
	 * @param id
	 * @return void
	 */
	public void setIDOrder(int id) {
		this.id_order = id;
	}
	
	/**
	 * setIDClient
	 * @param id
	 * @return void
	 */
	public void setIDClient(int id) {
		this.client_id = id;
	}
	
	/**
	 * setIDProduct
	 * @param id
	 * @return void
	 */
	public void setIDProduct(int id) {
		this.product_id = id;
	}
	
	/**
	 * setQuantity for order
	 * @param quantity
	 * @return void
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * getIDOrder
	 * @return integer
	 */
	public int getIDOrder() {
		return this.id_order;
	}
	
	/**
	 * getIDClient
	 * @return integer
	 */
	public int getIDClient() {
		return this.client_id;
	}
	
	/**
	 * getIDProduct
	 * @return integer
	 */
	public int getIDProduct() {
		return this.product_id;
	}
	
	/**
	 * getQuantity
	 * @return integer
	 */
	public int getQuantity(){
		return this.quantity;
	}
}
