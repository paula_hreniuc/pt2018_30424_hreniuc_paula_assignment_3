package model;

/**
 * This class contains variables that correspond to the fields of the products table from the database (have the same name), 2 constructors and setter and getters for each field
 * @author Paula Hreniuc
 *
 */
public class Products {
	private int id_product;
	private String name;
	private float price;
	private int quantity;
	
	/**
	 * Constructor 
	 * @param id
	 * @param name
	 * @param price
	 * @param quantity
	 */
	public Products(int id, String name, float price, int quantity){
		super();
		this.id_product = id;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}
	
	/**
	 * Constructor 
	 * @param name
	 * @param price
	 * @param quantity
	 */
	public Products(String name, float price, int quantity){
		super();
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		
	}
	
	/**
	 * setIdProduct
	 * @param id
	 * @return void
	 */
	public void setIdProduct(int id) {
		this.id_product = id;
	}
	
	/**
	 * setName product
	 * @param name
	 * @return void
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * setPrice product
	 * @param price
	 * @return void
	 */
	public void setPrice(float price) {
		this.price = price;
	}
	
	/**
	 * setQuantity product
	 * @param quantity
	 * @return void
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * getIdProduct
	 * @return integer
	 */
	public int getIdProduct() {
		return this.id_product;
	}
	
	/**
	 * getName of the product
	 * @return String
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * getPrice of the product
	 * @return float
	 */
	public float getPrice() {
		return this.price;
	}
	
	/**
	 * getQuantity
	 * @return integer
	 */
	public int getQuantity() {
		return this.quantity;
	}
}
