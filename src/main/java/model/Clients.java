package model;

/**
 * 
 * @author Paula Hreniuc
 *This class contains variables that correspond to the fields of the clients table from the database (have the same name), 2 constructors and setter and getters for each field
 */
public class Clients {
	private int id_client;
	private String name;
	private String address;
	private String phone;

	/**
	 * Constructor used when updating a client
	 * @param id
	 * @param name
	 * @param address
	 * @param phone
	 */
	public Clients(int id, String name, String address, String phone){
		super();
		this.id_client = id;
		this.name = name;
		this.address = address;
		this.phone = phone;		
	}

	/**
	 * Constructor used when inserting a client
	 * @param name
	 * @param address
	 * @param phone
	 */
	public Clients(String name, String address, String phone){
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;		
	}

	/**
	 * setIdCient 
	 * @param id
	 * @return void
	 */
	public void setIdCient(int id) {
		this.id_client = id;
	}

	/**
	 * setName to the client
	 * @param name
	 * @return void
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * setAddress to the client
	 * @param address
	 * @return void
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * setPhone client
	 * @param phone
	 * @return void
	 */
	public void setPhone(String phone) {
		this.address = phone;
	}

	/**
	 * getIdClient
	 * @return integer
	 */
	public int getIdClient() {
		return this.id_client;
	}

	/**
	 * getName of the client
	 * @return String
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * getAddress of the client
	 * @return String
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 * getPhone of the client
	 * @return String
	 */
	public String getPhone() {
		return this.phone;
	}
}
