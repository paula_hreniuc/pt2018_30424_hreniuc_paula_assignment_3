package presentation;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Clients;
import dataAccessLayer.WarehouseDAO;

/**
 * This class consists of the methods used to create a JTable 
 * @author Paula Hreniuc
 *
 */
public class ViewJTable extends JFrame{

	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor ViewJTable
	 * @param all
	 * @param columnsNames
	 */
	public ViewJTable (String[][] all, String[] columnsNames) {
		JTable table = new JTable(all,columnsNames); 
		setLayout(new FlowLayout());
		table.setPreferredScrollableViewportSize(new Dimension(500, 100));
		table.setFillsViewportHeight(true);
		
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane);
			
	}
	
	public JTable table (DefaultTableModel model) {
		JTable table = new JTable();
		setLayout(new FlowLayout());
		table.setPreferredScrollableViewportSize(new Dimension(500, 100));
		table.setFillsViewportHeight(true);
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane);
		
		return table;
			
	}
	
	public ViewJTable(ArrayList<Clients> list,String[] columnsNames) {
		DefaultTableModel model = new DefaultTableModel(columnsNames, 0);
		JTable table = table(model);
		Object[] row = new Object[4];
		for(int i = 0; i < list.size(); i++) {
			row[0] = list.get(i).getIdClient();
			row[1] = list.get(i).getName();
			row[2] = list.get(i).getAddress();
			row[3] = list.get(i).getPhone();
			
			model.addRow(row);
		}
	}
	
	/*	
	public static void retrieveProperties(Object object) {

		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true); // set modifier to public
			Object value;
			try {
				value = field.get(object);			
				System.out.println(field.getName() + "=" + value);

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		}
	}*/

	public static void main(String[] args) {
		Clients c1 = new Clients(1,"Ana", "Bucium", "0765443345");
		Clients c2 = new Clients(1,"Ana2", "Bucium", "0795443345");
		ArrayList<Clients> list = new ArrayList<Clients>();
		list.add(c1);
		list.add(c2);
		//String[][] all = {{"1", "Ana", "Bucium", "0763626616"},{"1", "Ana", "Plopilor", "0763626616"}};
		WarehouseDAO dao = new WarehouseDAO();
		String[][] all = {{}};
		all = dao.allCl();
		String[][] all2 = dao.allProd();
		String[] columnsNames2 = {"id", "name", "price", "quantity"};
		//String[] columnsNames = {"id", "name", "address", "phone"};
		//ViewJTable qui = new ViewJTable(list, columnsNames);
		//ViewJTable qui = new ViewJTable(all, columnsNames);
		ViewJTable qui2 = new ViewJTable(all2, columnsNames2);
		//qui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//qui.setSize(600,200);
		//qui.setVisible(true);
		//qui.setTitle("Jtable");
		
		qui2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		qui2.setSize(600,200);
		qui2.setVisible(true);
		qui2.setTitle("Jtable");
	}

}
