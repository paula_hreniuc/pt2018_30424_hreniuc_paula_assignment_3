package presentation;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * This class offers the methods that implement the main grafical user interface of our application
 * @author Paula Hreniuc
 *
 */
public class ViewAll extends JFrame implements ActionListener {
	JFrame frame  = new JFrame ("Operations");
	JPanel panel = new JPanel();
	JButton ClientOP = new JButton("Clients");
	JButton ProductOP = new JButton("Products");
	JButton Order = new JButton("Make an Order");
	
	/**
	 * Constructor ViewAll
	 * In this constructor we set the main properties of our frame and we add action listeners to the declared buttons
	 */
	public ViewAll() {
		frame.add(panel);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(600, 100);
		frame.setVisible(true);

		initialize();
		ClientOP.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					ViewClientOp vClients = new ViewClientOp();
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		ProductOP.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					ViewProductOp vProducts = new ViewProductOp();
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		Order.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					ViewOrderOp vProducts = new ViewOrderOp();
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
	}
	
	/**
	 * initialize method adds declared buttons to our panel 
	 * @return void
	 */
	private void initialize() {
		panel.setLayout(new GridLayout(1, 3, 5, 10));
		panel.add(ClientOP);
		panel.add(ProductOP);
		panel.add(Order);
	}
	
	public static void main (String args[]) {
		new ViewAll();	
	}

	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
