package presentation;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bll.ProductBLL;
import dataAccessLayer.WarehouseDAO;
import model.Products;

/**
 * This class offers the methods that implement the grafical user that will help the administrator add/edit/delete products from the database
 * @author Paula Hreniuc
 *
 */
public class ViewProductOp extends JFrame {

	private static final long serialVersionUID = 1L;

	JFrame frame  = new JFrame ("Products Operation");
	JPanel panel = new JPanel();

	JLabel name = new JLabel(":product name", JLabel.LEFT);
	JLabel price = new JLabel(":price", JLabel.LEFT);
	JLabel quantity = new JLabel(":quantity", JLabel.LEFT);
	JLabel id = new JLabel("ID: ", JLabel.LEFT);
	JLabel l1 = new JLabel("");
	JLabel l2 = new JLabel("");
	JTextField tf1 = new JTextField("");
	JTextField tf2 = new JTextField("");
	JTextField tf3 = new JTextField("");
	JTextField tf4 = new JTextField("");
	JButton AddProduct = new JButton("ADD");
	JButton DeleteProduct = new JButton("DELETE");
	JButton Edit = new JButton("EDIT");
	JButton ViewAllP = new JButton("View All Products");

	/**
	 * Constructor ViewProductOp
	 * In this constructor we set the main properties of our frame and we add action listeners to the declared buttons
	 * @exception Exception
	 */
	public ViewProductOp() {
		frame.add(panel);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(600, 300);
		frame.setVisible(true);

		initialize();
		//add action listener to the last button to see all products in A JTable 
		AddProduct.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String name = getName();
					float price = Float.parseFloat(getPrice());
					int quantity = Integer.parseInt(getQuantity());
					Products p = new Products(name, price, quantity);
					ProductBLL pbll = new ProductBLL();
					pbll.insertProduct(p);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		DeleteProduct.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					int id = Integer.parseInt(getId());
					ProductBLL pbll = new ProductBLL();
					pbll.deleteProduct(id);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		Edit.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					int id = Integer.parseInt(getId());
					String name = getName();
					float price = Float.parseFloat(getPrice());
					int quantity = Integer.parseInt(getQuantity());
					Products p = new Products(id, name, price, quantity);
					ProductBLL pbll = new ProductBLL();
					pbll.updateProduct(p);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		ViewAllP.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String[] columnsNames = {"id", "name", "price", "quantity"};
					WarehouseDAO dao = new WarehouseDAO();
					String[][] all = dao.allProd();
					ViewJTable qui = new ViewJTable(all, columnsNames);
					qui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					qui.setSize(600,200);
					qui.setVisible(true);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
	}

	/**
	 * initialize
	 * we add buttons/textFields/lables to the panel
	 * @return void
	 */
	private void initialize() {
		panel.setLayout(new GridLayout(7, 2, 5, 10));
		panel.add(tf1);
		panel.add(name);
		panel.add(tf2);
		panel.add(price);
		panel.add(tf3);
		panel.add(quantity);
		panel.add(AddProduct);
		panel.add(l1);
		panel.add(tf4);
		panel.add(id);
		panel.add(DeleteProduct);
		panel.add(l2);
		panel.add(Edit);
		panel.add(ViewAllP);
	}

	/**
	 * getName
	 * @return String
	 */
	public String getName() {
		return tf1.getText();
	}
	
	/**
	 * getPrice
	 * @return String
	 */
	public String getPrice() {
		return tf2.getText();
	}
	
	/**
	 * getQuantity
	 * @return String
	 */
	public String getQuantity() {
		return tf3.getText();
	}
	
	/**
	 * getId
	 * @return String
	 */
	public String getId() {
		return tf4.getText();
	}

	public static void main (String args[]) {
		new ViewProductOp();
	}
}
