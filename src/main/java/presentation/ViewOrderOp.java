package presentation;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bll.OrderBLL;
import dataAccessLayer.WarehouseDAO;
import model.Clients;
import model.Orders;
import model.Products;

/**
 * This class offers the methods that implement the grafical user that will help the client make an order
 * @author Paula Hreniuc
 *
 */
public class ViewOrderOp extends JFrame{

	private static final long serialVersionUID = 1L;
	JFrame frame  = new JFrame ("Client Operation");
	JPanel panel = new JPanel();

	JLabel client = new JLabel("Client id:", JLabel.LEFT);
	JLabel product = new JLabel("Product id", JLabel.LEFT);
	JLabel quantity = new JLabel("Quantity:", JLabel.LEFT);
	JLabel empty = new JLabel("");
	JTextField tf1 = new JTextField("");
	JTextField tf2 = new JTextField("");
	JTextField tf3 = new JTextField("");
	JButton clientsAndProducts = new JButton("View Clients and Products");
	JButton order = new JButton("Make your order");
	
	/**
	 * Constructor ViewOrderOp
	 * In this constructor we set the main properties of our frame and we add action listeners to the declared buttons
	 * @exception Exception
	 */
	public ViewOrderOp() {
		frame.add(panel);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(600, 180);
		frame.setVisible(true);

		initialize();
		order.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					int c_id = Integer.parseInt(getClientId());
					int p_id = Integer.parseInt(getProductId());
					int quantity = Integer.parseInt(getOrderedQuantity());
					
					WarehouseDAO dao = new WarehouseDAO();
					Products ret = dao.findProductsById(p_id);
					Clients c = dao.findClientById(c_id);
					if( c != null && ret != null && ret.getQuantity() >= quantity ) {
						Orders o = new Orders(c_id, p_id, quantity);
						OrderBLL obll = new OrderBLL();
						obll.insertO(o);
						
						//for bill
						String bill = "Your order has been made\n Name:" + c.getName() +"\nAddress:"+ c.getAddress() +"\nProduct:"+ ret.getName() + "\nQuantity:" + Integer.toString(quantity) +"\nTotal:"+  Float.toString(quantity*ret.getPrice());
						JOptionPane.showMessageDialog(frame, bill, "Bill", JOptionPane.INFORMATION_MESSAGE);
						print_receipt(bill);
						Products p = new Products(ret.getIdProduct(), ret.getName(), ret.getPrice(), ret.getQuantity() - quantity);
						if(dao.updateProduct(p) == false)
							JOptionPane.showMessageDialog(frame,"Could not update product", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else
					{
						if(c != null)
						{
							if (ret != null )
								JOptionPane.showMessageDialog(frame,"Not enough in stock", "Error", JOptionPane.ERROR_MESSAGE);
							else
								JOptionPane.showMessageDialog(frame,"Not such product", "Error", JOptionPane.ERROR_MESSAGE);
						}
						else
							JOptionPane.showMessageDialog(frame,"Not such client", "Error", JOptionPane.ERROR_MESSAGE);
					}
					
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		clientsAndProducts.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String[] columnsNames = {"id", "name", "price", "quantity"};
					WarehouseDAO dao = new WarehouseDAO();
					String[][] all = dao.allProd();
					ViewJTable qui = new ViewJTable(all, columnsNames);
					qui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					qui.setSize(600,200);
					qui.setVisible(true);
		
					String[] columnsNames2 = {"id", "name", "address", "phone"};
					String[][] all2 = dao.allCl();
					ViewJTable qui2 = new ViewJTable(all2, columnsNames2);
					qui2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					qui2.setSize(600,200);
					qui2.setVisible(true);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
	}
	
	/**
	 * initialize
	 * we add buttons/textFields to the panel
	 * @return void
	 */
	private void initialize() {
		panel.setLayout(new GridLayout(4, 2, 5, 10));
		panel.add(client);
		panel.add(tf1);
		panel.add(product);
		panel.add(tf2);
		panel.add(quantity);
		panel.add(tf3);
		panel.add(clientsAndProducts);
		panel.add(order);
	}
	
	/**
	 * getClientId
	 * @return String
	 */
	public String getClientId() {
		return tf1.getText();
	}
	
	/**
	 * getProductId
	 * @return String
	 */
	public String getProductId() {
		return tf2.getText();
	}
	
	/**
	 * getOrderedQuantity
	 * @return String
	 */
	public String getOrderedQuantity() {
		return tf3.getText();
	}
	
	public static void print_receipt(String text) {
		String fileName = "receipt.txt";
		PrintWriter outputStream;
		try {
			outputStream = new PrintWriter(fileName);
			outputStream.println(text);
			outputStream.close();
			System.out.println("Generated receipt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void main (String args[]) {
		new ViewOrderOp();
	}
}
