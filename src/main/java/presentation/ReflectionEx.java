package presentation;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import dataAccessLayer.WarehouseDAO;
import model.Clients;

public class ReflectionEx {

	public static void main (String args[]) {
		Class c = new WarehouseDAO().getClass();
		Method[] m = c.getMethods();
		
		System.out.println("There are " + m.length + " methods in this class");
		for (int i = 0; i < m.length; i++) {
			System.out.println(m[i]);
		}
		
		Class c2 = new Clients(1, "Marius", "Ariesului", "0754636622").getClass();
		Field[] f = c2.getFields();
		System.out.println("\nThere are " + f.length + " fields in this class");
		for (int i = 0; i < f.length; i++) {
			System.out.println(f[i]);
		}
		System.out.println("In our case the fields are set to private so they are not visible.");
	}
	
}
