package presentation;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Clients;
import bll.ClientBLL;
import dataAccessLayer.WarehouseDAO;

/**
 * This class offers the methods that implement the grafical user that will help the administrator add/edit/delete clients from the database
 * @author Paula Hreniuc
 *
 */
public class ViewClientOp extends JFrame {

	private static final long serialVersionUID = 1L;
	JFrame frame  = new JFrame ("Client Operation");
	JPanel panel = new JPanel();

	JLabel name = new JLabel(":name", JLabel.LEFT);
	JLabel address = new JLabel(":address", JLabel.LEFT);
	JLabel phone = new JLabel(":phone", JLabel.LEFT);
	JLabel id = new JLabel("ID: ", JLabel.LEFT);
	JLabel l1 = new JLabel("");
	JLabel l2 = new JLabel("");
	JTextField tf1 = new JTextField("");
	JTextField tf2 = new JTextField("");
	JTextField tf3 = new JTextField("");
	JTextField tf4 = new JTextField("");
	JButton AddClient = new JButton("ADD");
	JButton DeleteClient = new JButton("DELETE");
	JButton Edit = new JButton("EDIT");
	JButton ViewAllC = new JButton("View All Clients");

	/**
	 * Constructor ViewClientOp
	 * In this constructor we set the main properties of our frame and we add action listeners to the declared buttons
	 */
	public ViewClientOp() {
		frame.add(panel);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(600, 300);
		frame.setVisible(true);

		initialize();
		//here also an action listener to see all clients in a JTAbles
		
		AddClient.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String name = getName();
					String address = getAddress();
					String phone = getPhone();
					Clients c = new Clients(name, address, phone);
					ClientBLL cbll = new ClientBLL();
					cbll.insertClient(c);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		DeleteClient.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					int id = Integer.parseInt(getId());
					ClientBLL cbll = new ClientBLL();
					cbll.deleteClient(id);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		Edit.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					int id = Integer.parseInt(getId());
					String name = getName();
					String address = getAddress();
					String phone = getPhone();
					Clients c = new Clients(id, name, address, phone);
					ClientBLL cbll = new ClientBLL();
					cbll.updateClient(c);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		ViewAllC.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String[] columnsNames = {"id", "name", "address", "phone"};
					WarehouseDAO dao = new WarehouseDAO();
					String[][] all = dao.allCl();
					ViewJTable qui = new ViewJTable(all, columnsNames);
					qui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					qui.setSize(600,200);
					qui.setVisible(true);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
	}

	/**
	 * initialize
	 * we add buttons/textFields/Lables to the panel
	 * @return void
	 */
	private void initialize() {
		panel.setLayout(new GridLayout(7, 2, 5, 10));
		panel.add(tf1);
		panel.add(name);
		panel.add(tf2);
		panel.add(address);
		panel.add(tf3);
		panel.add(phone);
		panel.add(AddClient);
		panel.add(l1);
		panel.add(tf4);
		panel.add(id);
		panel.add(DeleteClient);
		panel.add(l2);
		panel.add(Edit);
		panel.add(ViewAllC);
	}

	/**
	 * getName
	 * @return String
	 */
	public String getName() {
		return tf1.getText();
	}
	
	/**
	 * getAddress
	 * @return String
	 */
	public String getAddress() {
		return tf2.getText();
	}
	
	/**
	 * getPhone
	 * @return String
	 */
	public String getPhone() {
		return tf3.getText();
	}
	
	/**
	 * getId
	 * @return String
	 */
	public String getId() {
		return tf4.getText();
	}

	public static void main (String args[]) {
		new ViewClientOp();
	}

}
