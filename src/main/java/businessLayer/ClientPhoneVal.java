package businessLayer;

import java.util.regex.Pattern;

import model.Clients;
/**
 * 
 * @author Paula Hreniuc
 * Class ClientPhoneVal implements Validator Interface. It consists of a method validate() that validates the Client's phone number
 * @param Clients
 * @exception (@throws IllegalArgumentException )
 * @return void
 */
public class ClientPhoneVal implements Validator<Clients> {
	private static final String PHONE_PATTERN = "[0-9]+";
	
	public void validate(Clients t) {
		Pattern pattern = Pattern.compile(PHONE_PATTERN);
		if (!pattern.matcher(t.getPhone()).matches()) {
			throw new IllegalArgumentException("Client phone not valid");
		}
	}
	
}
