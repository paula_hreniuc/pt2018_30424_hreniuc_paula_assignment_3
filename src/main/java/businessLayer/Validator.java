package businessLayer;
/**
 * 
 * @author Paula Hreniuc
 * Interface for Validators
 * @param <T>
 */
public interface Validator<T> {

	public void validate(T t);
}
