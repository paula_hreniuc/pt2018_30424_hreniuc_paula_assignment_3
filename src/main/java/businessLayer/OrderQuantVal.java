package businessLayer;

import model.Orders;
/**
 * 
 * @author Paula Hreniuc
 * Class OrderQuantVal implements Validator Interface. It consists of a method validate() that validates the product quantity
 * @param Orders
 * @exception (@throws IllegalArgumentException )
 * @return void
 */
public class OrderQuantVal implements Validator<Orders> {
	private static final int MIN_QUANT = 1;
	private static final int MAX_QUANT = 2000;
	
	public void validate(Orders t) {
		if (t.getQuantity() < MIN_QUANT || t.getQuantity() > MAX_QUANT ) {
			throw new IllegalArgumentException("Quantity not valid");
		}
	}
}
