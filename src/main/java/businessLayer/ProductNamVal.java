package businessLayer;

import java.util.regex.Pattern;

import model.Products;
/**
 * 
 * @author Paula Hreniuc
 * Class ProductNamVal implements Validator Interface. It consists of a method validate() that validates the product's name
 * @param Products
 * @exception (@throws IllegalArgumentException )
 * @return void
 */
public class ProductNamVal implements Validator<Products> {
private static final String NAME_PATTERN = "[a-zA-z]+([ '-][a-zA-Z]+)*";
	
	public void validate(Products t) {
		Pattern pattern = Pattern.compile(NAME_PATTERN);
		if (!pattern.matcher(t.getName()).matches()) {
			throw new IllegalArgumentException("Product name not valid");
		}
	}
}
