package businessLayer;

import model.Products;
/**
 * 
 * @author Paula Hreniuc
 * Class ProductQuantVal implements Validator Interface. It consists of a method validate() that validates the product's quantity
 * @param Products
 * @exception (@throws IllegalArgumentException )
 * @return void
 */
public class ProductQuantVal implements Validator<Products> {
	private static final int MIN_QUANT = 1;
	private static final int MAX_QUANT = 2000;
	
	public void validate(Products t) {
		if (t.getQuantity() < MIN_QUANT || t.getQuantity() > MAX_QUANT ) {
			throw new IllegalArgumentException("Quantity not valid");
		}
	}
}
