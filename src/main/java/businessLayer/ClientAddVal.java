package businessLayer;

import java.util.regex.Pattern;

import model.Clients;
/**
 * 
 * @author Paula Hreniuc
 * Class ClientAddVal implements Validator Interface. It consists of a method validate() that validates the Client's Address
 * @param Clients
 * @exception (@throws IllegalArgumentException )
 * @return void
 */
public class ClientAddVal implements Validator<Clients>{
	private static final String ADDRESS_PATTERN = "[a-zA-z]+([ '-][a-zA-Z]+)*";

	public void validate(Clients t) {
		Pattern pattern = Pattern.compile(ADDRESS_PATTERN);
		if (!pattern.matcher(t.getAddress()).matches()) {
			throw new IllegalArgumentException("Client address not valid");
		}
	}
}
