package businessLayer;

import model.Products;
/**
 * 
 * @author Paula Hreniuc
 * Class ProductPriceVal implements Validator Interface. It consists of a method validate() that validates the product's price
 * @param Products
 * @exception (@throws IllegalArgumentException )
 * @return void
 */
public class ProductPriceVal implements Validator<Products>{
	private static final float MIN_PRICE = (float) 0.1;
	private static final float MAX_PRICE = 1000;
	
	public void validate(Products t) {
		if (t.getPrice() < MIN_PRICE || t.getPrice() > MAX_PRICE ) {
			throw new IllegalArgumentException("Product price not valid!");
		}
	}
}
