package businessLayer;

import model.Orders;
/**
 * 
 * @author Paula Hreniuc
 * Class OrderPidVal implements Validator Interface. It consists of a method validate() that validates the product's ID
 * @param Orders
 * @exception (@throws IllegalArgumentException )
 * @return void
 */
public class OrderPidVal implements Validator<Orders> {
	private static final int MIN_O_ID = 1;
	
	public void validate(Orders t) {
		if (t.getIDProduct() < MIN_O_ID ) {
			throw new IllegalArgumentException("Id product is not valid");
		}
	}
}
