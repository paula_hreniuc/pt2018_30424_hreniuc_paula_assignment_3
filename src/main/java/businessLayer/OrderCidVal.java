package businessLayer;

import model.Orders;
/**
 * 
 * @author Paula Hreniuc
 * Class OrderCidVal implements Validator Interface. It consists of a method validate() that validates the client's ID
 * @param Orders
 * @exception (@throws IllegalArgumentException )
 * @return void
 */
public class OrderCidVal implements Validator<Orders> {
	private static final int MIN_C_ID = 1;

	public void validate(Orders t) {
		if (t.getIDClient() < MIN_C_ID ) {
			throw new IllegalArgumentException("Id client is not valid");
		}
	}
}
