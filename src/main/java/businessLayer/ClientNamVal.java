package businessLayer;

import java.util.regex.Pattern;

import model.Clients;
/**
 * 
 * @author Paula Hreniuc
 * Class ClientNamVal implements Validator Interface. It consists of a method validate() that validates the Client's name
 * @param Clients
 * @exception (@throws IllegalArgumentException )
 * @return void
 */
public class ClientNamVal implements Validator<Clients> {
	private static final String NAME_PATTERN = "[a-zA-z]+([ '-][a-zA-Z]+)*";
	
	public void validate(Clients t) {
		Pattern pattern = Pattern.compile(NAME_PATTERN);
		if (!pattern.matcher(t.getName()).matches()) {
			throw new IllegalArgumentException("Client namenot valid");
		}
	}
}
