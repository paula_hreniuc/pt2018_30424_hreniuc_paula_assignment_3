package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLayer.ProductNamVal;
import businessLayer.ProductPriceVal;
import businessLayer.ProductQuantVal;
import businessLayer.Validator;
import dataAccessLayer.WarehouseDAO;
import model.Products;

/**
 * This class contains methods that combine the validators methods with the database connections that provide the results of 
 * our given queries performed on the products table of our database
 * @author Paula Hreniuc
 *
 */
public class ProductBLL {
	private static List<Validator<Products>> validatorsP;
	
	public ProductBLL() {
		validatorsP = new ArrayList<Validator<Products>>();
		validatorsP.add(new ProductNamVal());
		validatorsP.add(new ProductPriceVal());
		validatorsP.add(new ProductQuantVal());		
	}
	
	/**
	 * findProductById
	 * @exception(@throws NoSuchElementException)
	 * @param id
	 * @return Products
	 */
	public static Products findProductById(int id) {
		Products p = WarehouseDAO.findProductsById(id);
		if (p == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return p;		
	}
	
	/**
	 * insertProduct
	 * @param Products object
	 * @return integer
	 */
	public static int insertProduct(Products p) {
		for(Validator<Products> v: validatorsP ) {
			v.validate(p);
		}
		return WarehouseDAO.insertProduct(p);
	}
	
	/**
	 * updateProduct
	 * @exception(@throws NoSuchElementException)
	 * @param Products object 
	 * @return boolean
	 */
	public static boolean updateProduct(Products p) {
		boolean ok = WarehouseDAO.updateProduct(p);
		if( ok == false) {
			throw new NoSuchElementException("could not update product with id=" + p.getIdProduct() );
		}
		return ok;
	}
	
	/**
	 * deleteProduct
	 * @exception(@throws NoSuchElementException)
	 * @param id
	 * @return boolean
	 */
	public static boolean deleteProduct(int id) {
		boolean ok = WarehouseDAO.deleteProduct(id);
		if( ok == false) {
			throw new NoSuchElementException("could not delete product with id=" + id );
		}
		return ok;
	}
	
	public static void main(String[] args) {
		
		Products p ;
		p = findProductById(1);
		System.out.println( p.getName() + " " + p.getPrice() + " " + p.getQuantity());
		if(updateProduct(p) == true) {
			System.out.println("updated");
		}
		
		if(deleteProduct(3) == true) {
			System.out.println("deleted");
		}
		
		ProductBLL bll = new ProductBLL();
		Products p2 = new Products("Ciocips", (float) (7.3), 50 );
		//int id = insertProduct(p2);
		insertProduct(p2);
		
	}
}
