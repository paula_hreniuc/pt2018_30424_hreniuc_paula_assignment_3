package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLayer.ClientAddVal;
import businessLayer.ClientNamVal;
import businessLayer.ClientPhoneVal;
import businessLayer.Validator;
import dataAccessLayer.WarehouseDAO;
import model.Clients;
/**
 * This class contains methods that combine the validators methods with the database connections that provide the results of 
 * our given queries performed on the clients table of our database
 * @author Paula Hreniuc
 *
 */
public class ClientBLL {

	private List<Validator<Clients>> validatorsC;
	
	/**
	 * Constructor ClientBLL
	 */
	public ClientBLL() {
		validatorsC = new ArrayList<Validator<Clients>>();
		validatorsC.add(new ClientNamVal());
		validatorsC.add(new ClientAddVal());
		validatorsC.add(new ClientPhoneVal());
	}
	
	/**
	 * findClientById
	 * @exception(@throws NoSuchElementException)
	 * @param id
	 * @return Clients object
	 */
	public static Clients findClientById(int id) {
		Clients c = WarehouseDAO.findClientById(id);
		if (c == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		return c;		
	}
	
	/**
	 * insertClient
	 * @param c
	 * @return integer 
	 */
	public int insertClient(Clients c) {
		for(Validator<Clients> v: validatorsC ) {
			v.validate(c);
		}
		return WarehouseDAO.insertClient(c);
	}
	
	/**
	 * updateClient
	 * @exception(@throws NoSuchElementException)
	 * @param c
	 * @return boolean
	 */
	public static boolean updateClient(Clients c) {
		boolean ok = WarehouseDAO.updateClient(c);
		if( ok == false) {
			throw new NoSuchElementException("could not update client with id=" + c.getIdClient() );
		}
		return ok;
	}
	
	/**
	 * deleteClient
	 * @exception(@throws NoSuchElementException)
	 * @param id
	 * @return boolean
	 */
	public static boolean deleteClient(int id) {
		boolean ok = WarehouseDAO.deleteClient(id);
		if( ok == false) {
			throw new NoSuchElementException("could not delete client with id=" + id );
		}
		return ok;
	}
	
	public static void main(String[] args) {
	//	Clients c2 = new Clients("Ana", "ceva", "0742414273");
		Clients c ;
		c = findClientById(1);
		System.out.println( c.getName() + " " + c.getAddress());
		if(updateClient(c) == true) {
			System.out.println("updated");
		}
		
		if(deleteClient(3) == true) {
			System.out.println("deleted");
		}
	//	insertClient(c2);
	}
	
}
