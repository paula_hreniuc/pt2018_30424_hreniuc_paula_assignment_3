package bll;

import java.util.ArrayList;
import java.util.List;

import businessLayer.OrderCidVal;
import businessLayer.OrderPidVal;
import businessLayer.OrderQuantVal;
import businessLayer.Validator;
import dataAccessLayer.WarehouseDAO;
import model.Orders;
/**
 * This class contains methods that combine the validators methods with the database connections
 * that provide the results of our given queries performed on the orders table of our database
 * @author Paula Hreniuc
 *
 */
public class OrderBLL {
	private static List<Validator<Orders>> validatorsO;
	
	/**
	 * Constructor OrderBLL
	 */
	public OrderBLL() {
		validatorsO = new ArrayList<Validator<Orders>>();
		validatorsO.add(new OrderCidVal());
		validatorsO.add(new OrderPidVal());
		validatorsO.add(new OrderQuantVal());
	}
	
	/**
	 * insertO
	 * @param Orders object
	 * @return integer
	 */
	public static int insertO(Orders o) {
		for(Validator<Orders> v: validatorsO ) {
			v.validate(o);
		}
		return WarehouseDAO.insertOrder(o);
	}
	
	public static void main(String[] args) {
		OrderBLL obll = new OrderBLL();
		Orders o = new Orders(2, 2, 30);
		System.out.println(insertO(o));
	}
}
