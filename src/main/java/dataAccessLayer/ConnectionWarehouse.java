package dataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * 
 * @author Paula Hreniuc
 * This class helps our application connect to the desired database.
 */
public class ConnectionWarehouse {
	private static final Logger LOGGER = Logger.getLogger(ConnectionWarehouse.class.getName());
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/warehouse?verifyServerCertificate=false&useSSL=true";
	private static final String USER = "root";
	private static final String PASS = "root";

	private static ConnectionWarehouse singleInstance = new ConnectionWarehouse();
	/**
	 * Constructor ConnectionWarehouse
	 */
	private ConnectionWarehouse() {
		try {
			Class.forName(DRIVER);
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	/**
	 * createConnection method returns on success the connection made to the database
	 * @exception(@throws SQLException )
	 * @return Connection
	 */
	private Connection createConnection() {
		Connection connection = null;

		try {
			connection = DriverManager
					.getConnection(DBURL,USER, PASS);

		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}

		return connection;
	}

	/**
	 * getConection it is a getter method 
	 * @return Connection
	 */
	public static Connection getConection() {
		return singleInstance.createConnection();
	}

	/**
	 * close method closes the database connection 
	 * @exception (@throws SQLException)
	 * @param connection
	 * @return void
	 */
	public static void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
			}
		}
	}

	/**
	 * close method that closes a statement
	 * @exception (@throws SQLException)
	 * @param statement
	 * @return void
	 */
	public static void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
			}
		}
	}

	/**
	 * close method that closes a resultSet
	 * @exception (@throws SQLException)
	 * @param statement
	 * @return void
	 */
	public static void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the ResultSet");
			}
		}
	}
}
