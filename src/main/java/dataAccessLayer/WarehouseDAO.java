package dataAccessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Clients;
import model.Orders;
import model.Products;
/**
 * 
 * @author Paula Hreniuc
 * This class consists of methods that perform queries on our database and retrieve
 * the desired data or return a value that tells us if the desired operation was successfully made.
 */
public class WarehouseDAO {

	protected static final Logger LOGGER = Logger.getLogger(WarehouseDAO.class.getName());
	private static final String insertStatementStringClients = "INSERT INTO clients (name,address,phone)"+ " VALUES (?,?,?)";
	private final static String findStatementStringClients = "SELECT * FROM clients where id_client = ?";
	private final static String updateStatementStringClients = "UPDATE clients SET name=?, address=?, phone=? WHERE id_client=?";
	private final static String deleteStatementStringClients = "DELETE FROM clients WHERE id_client=?";
	private final static String printingStatementClients = "SELECT * FROM clients";
	
	private static final String insertStatementStringProducts = "INSERT INTO products (name,price, quantity)"+ " VALUES (?,?,?)";
	private final static String findStatementStringProducts = "SELECT * FROM products where id_product = ?";
	private final static String updateStatementStringProducts  = "UPDATE products SET name=?, price=?, quantity=? WHERE id_product=?";
	private final static String deleteStatementStringProducts  = "DELETE FROM products WHERE id_product=?";
	private final static String printingStatementProducts = "SELECT * FROM products";
	
	private static final String insertStatementStringOrders = "INSERT INTO orders (client_id, product_id, quantity)"+ " VALUES (?,?,?)";

/**
 * On success findClientById() method returns the Clients object that was searched in the database by a given id
 * @exception(@throws SQLException )
 * @param ClientId
 * @returnClients
 */
	public static Clients findClientById(int ClientId) {
		Clients toReturn = null;
		
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementStringClients);
			findStatement.setLong(1, ClientId);
			rs = findStatement.executeQuery();
			rs.next();
			
			String name = rs.getString("name");
			String address = rs.getString("address");
			String phone = rs.getString("phone");
			return new Clients(ClientId, name, address, phone);
		}
		catch (SQLException e){
			LOGGER.log(Level.WARNING,"Warehouse: findClientById " + e.getMessage());
		}
		finally {
			ConnectionWarehouse.close(rs);
			ConnectionWarehouse.close(findStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return toReturn; 
	}
	
	/**
	 * insertClient() method inserts in the database a new client
	 * @exception(@throws SQLException )
	 * @param client
	 * @return integer 
	 */
	public static int insertClient(Clients client) {
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementStringClients, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getName());
			insertStatement.setString(2, client.getAddress());
			insertStatement.setString(3, client.getPhone());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getInt(1);
			}
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: insert client " + e.getMessage());
		}finally {
			ConnectionWarehouse.close(insertStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return insertedId;	
	}
	
	/**
	 * updateClient() method updates an existing client in the database with the newly provided data
	 * @exception(@throws SQLException )
	 * @param client
	 * @return boolean
	 */
	public static boolean updateClient(Clients client) {
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = dbConnection.prepareStatement(updateStatementStringClients);
			preparedStatement.setString(1, client.getName());
			preparedStatement.setString(2, client.getAddress());
			preparedStatement.setString(3, client.getPhone());
			preparedStatement.setInt(4, client.getIdClient());
			preparedStatement.execute();
			return true;
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: update client " + e.getMessage());
		}finally{
			ConnectionWarehouse.close(preparedStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return false;
	}
	
	/**
	 * deleteClient() method deletes a client from the database given its id
	 * @exception(@throws SQLException )
	 * @param id_client
	 * @return boolean 
	 */
	public static boolean deleteClient(int id_client) {
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementStringClients);
			deleteStatement.setInt(1, id_client);
			deleteStatement.execute();
			return true;
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: delete client " + e.getMessage());
		}finally {
			ConnectionWarehouse.close(deleteStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return false;
	}
	
	/**
	 * findProductsById() finds a product in our database given its id
	 * @exception(@throws SQLException )
	 * @param ProductId
	 * @return Products object
	 */
	public static Products findProductsById(int ProductId) {
		Products toReturn = null;
		
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementStringProducts);
			findStatement.setLong(1, ProductId);
			rs = findStatement.executeQuery();
			rs.next();
			
			String name = rs.getString("name");
			float price = rs.getFloat("price");
			int quantity = rs.getInt("quantity");
			return new Products(ProductId, name, price, quantity);
		}
		catch (SQLException e){
			LOGGER.log(Level.WARNING,"Warehouse: findClientById " + e.getMessage());
		}
		finally {
			ConnectionWarehouse.close(rs);
			ConnectionWarehouse.close(findStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return toReturn; 
	}
	
	/**
	 * insertProduct() method inserts a product in the database
	 * @exception(@throws SQLException )
	 * @param product
	 * @return integer
	 */
	public static int insertProduct(Products product) {
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementStringProducts, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, product.getName());
			insertStatement.setFloat(2, product.getPrice());
			insertStatement.setInt(3, product.getQuantity());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getInt(1);
			}
			
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: insert product " + e.getMessage());
		}
		finally {
			ConnectionWarehouse.close(insertStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return insertedId;
	}
	
	/**
	 * updateProduct() method updates an existing product with the newly provided data
	 * @exception(@throws SQLException )
	 * @param product
	 * @return boolean 
	 */
	public static boolean updateProduct(Products product) {
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = dbConnection.prepareStatement(updateStatementStringProducts);
			preparedStatement.setString(1, product.getName());
			preparedStatement.setFloat(2, product.getPrice());
			preparedStatement.setInt(3, product.getQuantity());
			preparedStatement.setInt(4, product.getIdProduct());
			preparedStatement.execute();
			return true;
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: update product " + e.getMessage());
		}finally {
			ConnectionWarehouse.close(preparedStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return false;
	}
	
	/**
	 * deleteProduct() method deletes a product from the database given its is
	 * @exception(@throws SQLException )
	 * @param id_product
	 * @return boolean
	 */
	public static boolean deleteProduct(int id_product) {
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementStringProducts);
			deleteStatement.setInt(1, id_product);
			deleteStatement.execute();
			return true;
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: delete product " + e.getMessage());
		}finally {
			ConnectionWarehouse.close(deleteStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return false;
	}
	
	/**
	 * allClients() method returns in an ArrayList structure all the clients in the database up to the given calling point of the method 
	 * @exception(@throws SQLException )
	 * @return ArrayList<Clients> 
	 */
	public ArrayList<Clients> allClients(){
		ArrayList<Clients> allClients = new ArrayList<Clients>();
		
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement printStatement = null;
		try {
			Statement st = dbConnection.createStatement();
			ResultSet rs = st.executeQuery(printingStatementClients);
			Clients client;
			while(rs.next()) {
				client = new Clients(rs.getInt("id_client"), rs.getString("name"), rs.getString("address"), rs.getString("phone"));
				allClients.add(client);
			}
					
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: couldn't print clients " + e.getMessage());
		}finally {
			ConnectionWarehouse.close(printStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return allClients;
	}
	
	/**
	 * allProducts() method returns in an ArrayList structure all the products in the database up to the given calling point of the method
	 * @exception(@throws SQLException )
	 * @return ArrayList<Products>
	 */
	public ArrayList<Products> allProducts(){
		ArrayList<Products> allProducts = new ArrayList<Products>();		
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement printStatement = null;
		try {
			Statement st = dbConnection.createStatement();
			ResultSet rs = st.executeQuery(printingStatementProducts);
			Products product;
			while(rs.next()) {
				product = new Products(rs.getInt("id_product"), rs.getString("name"), rs.getFloat("price"), rs.getInt("quantity"));
				allProducts.add(product);
			}
					
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: couldn't print products " + e.getMessage());
		}finally {
			ConnectionWarehouse.close(printStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return allProducts;
	}
	
	/**
	 * allProd() method returns an array of strings of all the products in the database up to the given calling point of the method
	 * @exception(@throws SQLException )
	 * @return String[][]
	 */
	public String[][] allProd(){
		String[][] all =  new String[100][4] ;
		int j = 0;
		
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement printStatement = null;
		try {
			Statement st = dbConnection.createStatement();
			ResultSet rs = st.executeQuery(printingStatementProducts);
			while(rs.next()) {
				all[j][0] = Integer.toString(rs.getInt("id_product")); 
				all[j][1] = rs.getString("name") ;
				all[j][2] = Float.toString(rs.getFloat("price")) ;
				all[j][3] = Integer.toString(rs.getInt("quantity"));
				j++;
			}
					
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: couldn't print products " + e.getMessage());
		}finally {
			ConnectionWarehouse.close(printStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return all;
	}
	
	/**
	 * allCl() method returns an array of strings of all the clients in the database up to the given calling point of the method 
	 * @exception(@throws SQLException )
	 * @return String[][]
	 */
	public String[][] allCl(){
		int j = 0;
		String[][] all = new String[100][4] ;
		
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement printStatement = null;
		try {
			Statement st = dbConnection.createStatement();
			ResultSet rs = st.executeQuery(printingStatementClients);
			while(rs.next()) {
				all[j][0] = Integer.toString(rs.getInt("id_client")) ;
				all[j][1] = rs.getString("name") ;
				all[j][2] =  rs.getString("address") ;
				all[j][3] =  rs.getString("phone") ;
				j++;
			}
					
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: couldn't print clients " + e.getMessage());
		}finally {
			ConnectionWarehouse.close(printStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return all;
	}
	
	/**
	 * insertOrder() method inserts an order in the database 
	 * @exception(@throws SQLException )
	 * @param order
	 * @return integer
	 */
	public static int insertOrder(Orders order) {
		Connection dbConnection = ConnectionWarehouse.getConection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementStringOrders, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getIDClient());
			insertStatement.setInt(2, order.getIDProduct());
			insertStatement.setInt(3, order.getQuantity());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getInt(1);
			}
			
		}catch(SQLException e) {
			LOGGER.log(Level.WARNING, "WarehouseDAO: insert order " + e.getMessage());
		}
		finally {
			ConnectionWarehouse.close(insertStatement);
			ConnectionWarehouse.close(dbConnection);
		}
		return insertedId;
	}
}
